# <div align="center">Beer Box - a box for web development</div>

🚀 Quick and easy development environment

👨🏻‍💻 Focus on your work

🍺 Have a beer!

##

Beer Box is a simple Vagrant base box inspired by Scotch Box. I made it since Scotch Box was getting outdated and I needed it for quick to set up dev environment for my university class.

## Usage

```sh
git clone https://gitlab.com/dusansimic/beerbox.git
cd beerbox
vagrant up
```

## Features

* Ubuntu 20.04 LTS
* PHP 7.4
*	MySQL 8
* Apache2
* Node.js 14
* Yarn 1.22

## Author

[Dušan Simić](https://dusansimic.me) <<dusan.simic1810@gmail.com>>

## 🤝🏻 Contributing

If you want some service, language or database to be added, open an issue and describe why you think it should be added.


## 📝 License

Copyright © 2020 [Dušan Simić](https://dusansimic.me).

For licenses of the operating system and included software, look them up.
